# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.db import models

class BaseManagementItem(models.Model):
    view_uid = ''  # has to be same as in LFSBaseManagementView

    def get_update_url(self):
        return reverse('lfs_update_%s' % (self.view_uid), kwargs={'id': self.pk})

    def get_delete_url(self):
        return reverse('lfs_delete_%s' % (self.view_uid), kwargs={'id': self.pk})

    def get_view_url(self):
        """ Public view of this item. This view doesn't exist by default!
        """
        return reverse('lfs_view_%s' % (self.view_uid), kwargs={'id': self.pk})

    class Meta:
        abstract = True
# -*- coding: utf-8 -*-
from functools import update_wrapper

# django imports
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.http import require_POST

# lfs imports
import lfs.core.utils


class LFSBaseManagementView(object):
    view_uid = 'lfs_bmv'
    template_update_item = "management_items/update_item.html"
    template_add_item = "management_items/add_item.html"

    # CLASS getters
    def get_model_cls(self):
        """ Return Model class that should inherit from BaseManagementItem
        """
        raise Exception("Not implemented")

    def get_add_form_cls(self):
        """ Return form used by add view """
        raise Exception("Not implemented")

    def get_edit_form_cls(self):
        """ Return form used by edit view """
        return self.get_add_form_cls()
    # /CLASS getters

    # MESSAGES
    def get_item_updated_message(self, item):
        """ Return message that will be shown after item is updated by user
        """
        return _(u"Item has been saved.")

    def get_item_added_message(self, item):
        """ Return message that will be shown after item is added
        """
        return _(u"Item has been added.")

    def get_item_deleted_message(self, item):
        """ Return message that will be shown after item is deleted
        """
        return _(u"Item has been deleted.")
    # /MESSAGES

    # VIEWS
    def manage_items(self, request):
        """ Dispatches to the first item or to the form to add a item (if there is no
            item yet).
        """
        klass = self.get_model_cls()
        try:
            obj = klass.objects.all()[0]
            url = reverse("lfs_update_%s" % (self.view_uid), kwargs={"id": obj.pk})
        except IndexError:
            url = reverse("lfs_add_%s" % (self.view_uid))

        return HttpResponseRedirect(url)

    def manage_add_item(self, request):
            """Provides a form to add a new page.
            """
            klass = self.get_model_cls()
            form_klass = self.get_add_form_cls()

            if request.method == "POST":
                form = form_klass(data=request.POST, files=request.FILES)
                if form.is_valid():
                    item = form.save()
                    self.update_positions()

                    return lfs.core.utils.set_message_cookie(
                        url=reverse("lfs_update_%s" % (self.view_uid), kwargs={"id": item.pk}),
                        msg=self.get_item_added_message(item),
                    )
            else:
                form = form_klass()

            return render_to_response(self.template_add_item, RequestContext(request, {
                "form": form,
                "add_item_url": reverse("lfs_add_%s" % (self.view_uid)),
                "items": klass.objects.all(),
            }))

    def manage_update_item(self, request, id):
        """ Provides a form to edit the item with the passed id.
        """
        klass = self.get_model_cls()
        form_klass = self.get_edit_form_cls()

        item = get_object_or_404(klass, pk=id)
        if request.method == "POST":
            form = form_klass(instance=item, data=request.POST, files=request.FILES)
            if form.is_valid():
                new_item = form.save()
                self.update_positions()

                # delete file
                if request.POST.get("delete_file"):
                    item.file.delete()

                return lfs.core.utils.set_message_cookie(
                    url=reverse("lfs_update_%s" % (self.view_uid), kwargs={"id": item.id}),
                    msg=self.get_item_updated_message(item),
                )
        else:
            form = form_klass(instance=item)

        return render_to_response(self.template_update_item, RequestContext(request, {
            "item": item,
            "items": klass.objects.all(),
            "add_item_url": reverse("lfs_add_%s" % (self.view_uid)),
            "form": form,
            "current_id": int(id),
        }))

    def manage_delete_item(self, request, id):
        """Deletes the item with passed id.
        """
        klass = self.get_model_cls()
        item = get_object_or_404(klass, pk=id)
        item.delete()

        return lfs.core.utils.set_message_cookie(
            url=reverse("lfs_manage_%s" % (self.view_uid)),
            msg=self.get_item_deleted_message(item),
        )

    def view_item(self, request, id):
        """ Public view of this item. To be implemented in descendant classes
        """
        raise Http404
    # /VIEWS

    # URLS
    # methods below are based on contrib.admin.sites
    def has_permission(self, request):
        """
        Returns True if the given HttpRequest has permission to view
        *at least one* page in the admin site.
        """
        return request.user.is_active and request.user.has_perm('core.manage_shop')

    def items_view(self, view, cacheable=False):
        """
        Decorator to create an view attached to this ``LFSBaseManagementView``. This
        wraps the view and provides permission checking by calling
        ``self.has_permission``.

        """
        def inner(request, *args, **kwargs):
            if not self.has_permission(request):
                return HttpResponseRedirect('%s?next=%s' % (reverse('django.contrib.auth.views.login'), request.path))
            return view(request, *args, **kwargs)
        if not cacheable:
            inner = never_cache(inner)
        # We add csrf_protect here so this function can be used as a utility
        # function for any view, without having to repeat 'csrf_protect'.
        if not getattr(view, 'csrf_exempt', False):
            inner = csrf_protect(inner)
        return update_wrapper(inner, view)

    def get_wrap(self):
        """ Return wrapper method for urls.
        """
        def wrap(view, cacheable=False):
            def wrapper(*args, **kwargs):
                return self.items_view(view, cacheable)(*args, **kwargs)
            return update_wrapper(wrapper, view)
        return wrap

    def get_urls(self):
        from django.conf.urls import patterns, url, include

        wrap = self.get_wrap()

        urlpatterns = patterns('',
            url(r'^lfs_add_%s/$' % (self.view_uid), wrap(self.manage_add_item), name="lfs_add_%s" % (self.view_uid)),
            url(r'^lfs_update_%s/(?P<id>\d+)/$' % (self.view_uid), wrap(self.manage_update_item),
                                                                   name="lfs_update_%s" % (self.view_uid)),
            url(r'^lfs_delete_%s/(?P<id>\d*)/$' % (self.view_uid), require_POST(wrap(self.manage_delete_item)),
                                                                   name="lfs_delete_%s" % (self.view_uid)),
            url(r'^lfs_manage_%s/$' % (self.view_uid), wrap(self.manage_items), name="lfs_manage_%s" % (self.view_uid)),
            url(r'^lfs_view_%s/(?P<id>\d*)/$' % (self.view_uid), self.view_item, name="lfs_view_%s" % (self.view_uid)),
        )
        return urlpatterns

    @property
    def urls(self):
        return self.get_urls()
    # /URLS

    # UTILS
    def update_positions(self):
        """Updates the positions of all items.
        """
        klass = self.get_model_cls()
        for i, item in enumerate(klass.objects.all()):
            item.position = (i + 1) * 10
            item.save()
    # /UTILS
